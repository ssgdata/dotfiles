#!/usr/bin/env bash

#  Copyright © ___2021___ ___Joseph Baek___
# MIT License

location=/Users/$(whoami)

read -p "[git] git username? :" username
read -p "[git] git address? :" address
read -r -s -p "[sudo] sudo password for $(whoami):" password

# installing xcode
if [ ! -f /usr/bin/xcode-select ]; then
  xcode-select --install
fi

# Homebrew
if [ ! -f /opt/homebrew/bin/brew ]; then
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> $location/.zshrc
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Launchpad
defaults write com.apple.dock springboard-columns -int 8
defaults write com.apple.dock springboard-rows -int 8
default write com.apple.dock ResetLaunchPad -bool true
killall Dock

# brew bundle 
brew bundle --file common.brewfile

## for security
sudo xattr -dr com.apple.quarantine /Applications/Notion.app
sudo xattr -dr com.apple.quarantine /Applications/Slack.app

brew bundle cleanup --global --force

# macOS Settings
defaults write com.apple.dock autohide -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerDrag -bool true
defaults write com.apple.AppleMultitouchTrackpad TrackpadThreeFingerDrag -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
defaults write com.apple.finder ShowStatusBar -bool true
defaults write com.apple.finder ShowPathbar -bool true
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"
defaults write NSGlobalDomain KeyRepeat -int 1
defaults write NSGlobalDomain InitialKeyRepeat -int 10

# oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# git config
git config --global user.name "$username"
git config --global user.email "$address"
git config --global core.editore "code -w"
git config --global merge.tool code

# ruby
rbenv install 2.7.5
rbenv global 2.7.5


# settings
$(brew --prefix)/opt/fzf/install

fasd_cache="$HOME/.fasd-init-bash"
if [ "$(command -v fasd)" -nt "$fasd_cache" -o ! -s "$fasd_cache" ]; then
  fasd --init posix-alias bash-hook bash-ccomp bash-ccomp-install >| "$fasd_cache"
fi
source "$fasd_cache"
unset fasd_cache

# neovim plugins
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# setting file move
mkdir -p ~/.config/nvim
cp ./settings/.zshrc ~/.zshrc
cp ./settings/init.vim ~/.config/nvim/init.vim
cp ./settings/.sshdexp ~/.sshdexp
cp ./settings/.tmux.conf ~/.tmux.conf
