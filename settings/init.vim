call plug#begin('~/.vim/plugged')

Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
Plug 'fatih/vim-go', { 'tag': '*' }
Plug 'thaerkh/vim-indentguides'

call plug#end()
set encoding=UTF-8
set autoindent
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
set number
set ruler
set title
set wrap
set cursorline
set mouse=a
set clipboard=unnamed
let g:indentguides_spacechar = '┆' 
let g:indentguides_tabchar = '|' 
let g:indent_guides_enable_on_vim_startup = 1 
let g:indent_guides_start_level=2 
let g:indent_guides_guide_size=1
let g:fzf_install = 'yes | ./install'
