# powerline 10k
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# user export
export ZSH="$HOME/.oh-my-zsh"
export GOPATH=$HOME/Works/environments/gopath
export NLS_LANG="AMERICAN_AMERICA.US7ASCII"
export ORACLE_HOME="$HOME/Works/environments/instantclient_19_8"
export TNS_ADMIN=$ORACLE_HOME/network/admin
export PATH=$ORACLE_HOME:$HOME/Works/environments/gopath/bin:/opt/homebrew/bin:$PATH
export SCOMPASSWORD="1Rhcrp@ahrvh"

# theme
ZSH_THEME="powerlevel10k/powerlevel10k"
#ZSH_THEME="agnoster"
#ZSH_THEME="common"

# zsh plugins
plugins=( 
	git 
	yarn 
	node 
	brew 
	docker 
	zsh-syntax-highlighting 
	zsh-autosuggestions 
	tmux 
	tmuxinator
	fzf
	fasd
)

# user aliases
alias vi="nvim"
alias vim="nvim"
alias vimdiff="nvim -d"
alias cat="bat"
alias top="htop"
alias mux="tmuxinator"

# oh-my-zsh source
source $ZSH/oh-my-zsh.sh

# powerline 10k
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# rbenv set
[[ -d ~/.rbenv  ]] && \
  export PATH=${HOME}/.rbenv/bin:${PATH} && \
  eval "$(rbenv init -)"
